module gitlab.com/albertodominguez/go-audius

go 1.17

require (
	go.uber.org/zap v1.21.0
	golang.org/x/tools v0.1.9
	honnef.co/go/tools v0.2.2
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/sys v0.0.0-20211019181941-9d821ace8654 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
