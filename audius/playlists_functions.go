package audius

import (
	"fmt"
	"net/http"
)

func (c *Client) readPlaylist(URL *EndpointURL) (*Playlist, error) {
	var res struct {
		BaseResponse
		Data *Playlist `json:"data"`
	}

	if err := c.apiCall(&res, http.MethodGet, URL); err != nil {
		c.logger.Errorw(
			"unexpected error while loading single playlist from Audius",
			"error", err)
		return nil, err
	}

	return res.Data, nil
}

func (c *Client) readPlaylists(URL *EndpointURL) ([]Playlist, error) {
	var res struct {
		BaseResponse
		Data []Playlist `json:"data"`
	}

	if err := c.apiCall(&res, http.MethodGet, URL); err != nil {
		c.logger.Errorw(
			"unexpected error while loading multiple playlists from Audius",
			"error", err)
		return nil, err
	}

	return res.Data, nil
}

func (c *Client) GetPlaylist(playlistID string) (*Playlist, error) {
	URL, err := c.ServerEndpointURL(fmt.Sprintf("/v1/playlists/%s", playlistID))
	if err != nil {
		return nil, err
	}

	return c.readPlaylist(URL)
}

func (c *Client) GetPlaylistTracks(playlistID string) ([]Track, error) {
	URL, err := c.ServerEndpointURL(fmt.Sprintf("/v1/playlists/%s/tracks", playlistID))
	if err != nil {
		return nil, err
	}

	return c.readTracks(URL)
}

func (c *Client) SearchPlaylist(query string) ([]Playlist, error) {
	URL, err := c.ServerEndpointURL("/v1/playlists/search")
	if err != nil {
		return nil, err
	}

	URL.AddQueryParameter("query", query)
	return c.readPlaylists(URL)
}
