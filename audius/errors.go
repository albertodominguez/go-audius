package audius

import "errors"

var (
	ErrNoServersAvailable = errors.New("AUDIUSERR_NO_SERVER_AVAILABLE")
)
