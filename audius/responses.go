package audius

import (
	"fmt"
	"math/rand"
)

type Response interface {
	Error() error
}

type BaseResponse struct {
	ErrMessage string `json:"message,omitempty"`
}

func (r BaseResponse) Error() error {
	if len(r.ErrMessage) == 0 {
		return nil
	}

	return fmt.Errorf(r.ErrMessage)
}

type ServersResponse struct {
	BaseResponse
	AvailableServers []string `json:"data"`
}

func (r ServersResponse) Get(preferredServers ...string) (string, error) {
	if len(r.AvailableServers) == 0 {
		return "", ErrNoServersAvailable
	}

	for _, p := range preferredServers {
		for _, s := range r.AvailableServers {
			if p == s {
				return s, nil
			}
		}
	}

	return r.AvailableServers[rand.Intn(len(r.AvailableServers))], nil
}
