package audius

import (
	"errors"
	"fmt"
	"testing"
)

func TestClient_SetDefaultServer(t *testing.T) {
	c, err := New("Unitea", ModeDevelopment)
	if err != nil {
		t.Fatal(err)
		return
	}

	if c == nil {
		t.Fatal(errors.New("unable to get a valid API client"))
		return
	}

	err = c.SetDefaultServer()
	if err != nil {
		t.Fatal(err)
	}
}

func TestClient_SearchUser(t *testing.T) {
	c, _ := New("Unitea", ModeDevelopment)
	c.SetDefaultServer()

	tracks, err := c.SearchUsers("Brown")
	if err != nil {
		t.Fatal(err)
	}

	if len(tracks) == 0 {
		t.Fatal(fmt.Errorf("unexpected result for user search"))
	}
}

func TestClient_SearchPlaylist(t *testing.T) {
	c, _ := New("Unitea", ModeDevelopment)
	c.SetDefaultServer()

	tracks, err := c.SearchPlaylist("Beat")
	if err != nil {
		t.Fatal(err)
	}

	if len(tracks) == 0 {
		t.Fatal(fmt.Errorf("unexpected result for playlists search"))
	}
}
func TestClient_SearchTracks(t *testing.T) {
	c, _ := New("Unitea", ModeDevelopment)
	c.SetDefaultServer()

	tracks, err := c.SearchTracks("b2b", false)
	if err != nil {
		t.Fatal(err)
	}

	if len(tracks) == 0 {
		t.Fatal(fmt.Errorf("unexpected result for track search"))
	}
}
