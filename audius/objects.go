package audius

type Cover struct {
	Small *string `json:"640x,omitempty"`
	Large *string `json:"2000x,omitempty"`
}

type Picture struct {
	Thumbnail *string `json:"150x150,omitempty"`
	Small     *string `json:"480x480,omitempty"`
	Large     *string `json:"1000x1000,omitempty"`
}

type User struct {
	ID             string   `json:"id"`
	Handle         string   `json:"handle"`
	Name           string   `json:"name"`
	Bio            string   `json:"bio"`
	CoverPhoto     *Cover   `json:"cover_photo"`
	ProfilePicture *Picture `json:"profile_picture"`

	IsVerifiedAccount bool  `json:"is_verified"`
	Followers         int64 `json:"follower_count"`
	Followees         int64 `json:"followee_count"`
}

type Track struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Duration    int    `json:"duration"`
	ReleaseDate string `json:"release_date"`
	Mood        string `json:"mood"`
	Genre       string `json:"genre"`

	IsDownloadable bool  `json:"downloadable"`
	PlaysCount     int64 `json:"play_count"`
	LikesCount     int64 `json:"favorite_count"`
	SharesCount    int64 `json:"repost_count"`

	Artwork *Picture `json:"artwork"`
	Artist  *User    `json:"user,omitempty"`
}

type Playlist struct {
	ID          string `json:"id"`
	Title       string `json:"playlist_name"`
	Description string `json:"description"`

	IsAlbum     bool  `json:"is_album"`
	TracksCount int64 `json:"track_count"`
	PlaysCount  int64 `json:"total_play_count"`
	LikesCount  int64 `json:"favorite_count"`
	SharesCount int64 `json:"repost_count"`

	Artwork *Picture `json:"artwork"`
	Creator *User    `json:"user,omitempty"`
}
