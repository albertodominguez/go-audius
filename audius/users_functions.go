package audius

import (
	"fmt"
	"net/http"
)

func (c *Client) readUser(URL *EndpointURL) (*User, error) {
	var res struct {
		BaseResponse
		Data *User `json:"data"`
	}

	if err := c.apiCall(&res, http.MethodGet, URL); err != nil {
		c.logger.Errorw(
			"unexpected error while loading single user from Audius",
			"error", err)
		return nil, err
	}

	return res.Data, nil
}

func (c *Client) readUsers(URL *EndpointURL) ([]User, error) {
	var res struct {
		BaseResponse
		Data []User `json:"data"`
	}

	if err := c.apiCall(&res, http.MethodGet, URL); err != nil {
		c.logger.Errorw(
			"unexpected error while loading multiple users from Audius",
			"error", err)
		return nil, err
	}

	return res.Data, nil
}

func (c *Client) GetUser(userID string) (*User, error) {
	URL, err := c.ServerEndpointURL(fmt.Sprintf("/v1/users/%s", userID))
	if err != nil {
		return nil, err
	}

	return c.readUser(URL)
}

func (c *Client) GetUserTracks(userID string) ([]Track, error) {
	URL, err := c.ServerEndpointURL(fmt.Sprintf("/v1/users/%s/tracks", userID))
	if err != nil {
		return nil, err
	}

	return c.readTracks(URL)
}

func (c *Client) GetUserFavoriteTracks(userID string) ([]Track, error) {
	URL, err := c.ServerEndpointURL(fmt.Sprintf("/v1/users/%s/favorites", userID))
	if err != nil {
		return nil, err
	}

	return c.readTracks(URL)
}

func (c *Client) SearchUsers(query string) ([]User, error) {
	URL, err := c.ServerEndpointURL("/v1/users/search")
	if err != nil {
		return nil, err
	}
	URL.AddQueryParameter("query", query)

	return c.readUsers(URL)
}
