package audius

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"go.uber.org/zap"
)

type Client struct {
	AppName string
	Server  string

	httpClient *http.Client
	logger     *zap.SugaredLogger
}

const (
	ServersEndpoint = "https://api.audius.co" // Default initial server
	ModeProduction  = "prd"
	ModeDevelopment = "dev"
)

// New Returns a new API client using given AppName.
func New(app string, mode string, preferredServers ...string) (*Client, error) {
	var logger *zap.Logger
	var err error

	var client *Client = &Client{
		AppName:    app,
		httpClient: &http.Client{Timeout: time.Minute},
	}

	switch mode {
	case ModeProduction:
		logger, err = zap.NewProduction()
	case ModeDevelopment:
		logger, err = zap.NewDevelopment()
	}

	if err != nil {
		return nil, err
	}

	defer logger.Sync()
	client.logger = logger.Sugar()

	err = client.SetDefaultServer(preferredServers...)
	if err != nil {
		client.logger.Fatalw(
			"unable to get servers list from Audius",
			"error", err)
		return nil, err
	}

	return client, nil
}

func (c *Client) SetDefaultServer(preferredServers ...string) error {
	URL, _ := NewEndpointURL(ServersEndpoint)

	res := ServersResponse{}
	err := c.call(&res, http.MethodGet, URL)
	if err != nil {
		return err
	}

	srv, err := res.Get(preferredServers...)
	if err != nil {
		c.logger.Errorw(
			"unable to set a default server from Audius",
			"error", err)
		return err
	}

	c.logger.Debugw(
		"setting a default server",
		"server", srv)
	c.Server = srv
	return nil
}

func (c *Client) ServerEndpointURL(path string) (*EndpointURL, error) {
	return NewEndpointURL(c.Server + path)
}

func (c *Client) apiCall(r interface{}, method string, URL *EndpointURL) error {
	if !URL.HasQueryParameter("app_name") {
		URL.AddQueryParameter("app_name", c.AppName)
	}

	c.logger.Debugw(
		"attemping to call Audius API",
		"URL", URL.String())

	return c.call(r, method, URL)
}

func (c *Client) call(r interface{}, method string, URL *EndpointURL) error {
	req, err := http.NewRequest(method, URL.String(), nil)
	if err != nil {
		c.logger.Errorw(
			"unable to create the HTTP request",
			"error", err)

		return err
	}

	req.Header.Add("Accept", "application/json")
	res, err := c.httpClient.Do(req)
	if res == nil {
		log.Printf("unexpected nil response from API")
	}

	if err != nil {
		c.logger.Errorw(
			"unable to execute the HTTP request",
			"error", err)
		return err
	}

	defer res.Body.Close()
	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		c.logger.Errorw(
			"unable to load the API response",
			"error", err)
		return err
	}

	// c.logger.Debugw(
	// 	"API Response",
	// 	"Body", b)

	err = json.Unmarshal(b, r)
	if err != nil {
		c.logger.Errorw(
			"unexpected error while unmarshaling API response object",
			"error", err)

		return err
	}

	return nil
}
