package audius

import "net/url"

type EndpointURL struct {
	URL *url.URL
}

func (r *EndpointURL) HasQueryParameter(key string) bool {
	values := r.URL.Query()
	return values.Has(key)
}

func (r *EndpointURL) AddQueryParameter(key, value string) {
	values := r.URL.Query()
	values.Add(key, value)
	r.URL.RawQuery = values.Encode()
}

func (r *EndpointURL) RemoveQueryParameter(key string) {
	values := r.URL.Query()
	values.Del(key)
	r.URL.RawQuery = values.Encode()
}

func (r *EndpointURL) String() string {
	return r.URL.String()
}

func NewEndpointURL(URL string) (*EndpointURL, error) {

	tURL, err := url.Parse(URL)
	if err != nil {
		return nil, err
	}

	return &EndpointURL{
		URL: tURL,
	}, nil
}
