package audius

import (
	"fmt"
	"net/http"
)

func (c *Client) readTrack(URL *EndpointURL) (*Track, error) {
	var res struct {
		BaseResponse
		Data *Track `json:"data"`
	}

	if err := c.apiCall(&res, http.MethodGet, URL); err != nil {
		c.logger.Errorw(
			"unexpected error while loading single track from Audius",
			"error", err)
		return nil, err
	}

	return res.Data, nil
}

func (c *Client) readTracks(URL *EndpointURL) ([]Track, error) {
	var res struct {
		BaseResponse
		Data []Track `json:"data"`
	}

	if err := c.apiCall(&res, http.MethodGet, URL); err != nil {
		c.logger.Errorw(
			"unexpected error while loading multiple tracks from Audius",
			"error", err)
		return nil, err
	}

	return res.Data, nil
}

func (c *Client) GetTrack(trackID string) (*Track, error) {
	URL, err := c.ServerEndpointURL(fmt.Sprintf("/v1/tracks/%s", trackID))
	if err != nil {
		return nil, err
	}

	return c.readTrack(URL)
}

func (c *Client) GetTrendingTracks() ([]Track, error) {
	URL, err := c.ServerEndpointURL("/v1/tracks/trending")
	if err != nil {
		return nil, err
	}

	return c.readTracks(URL)
}

func (c *Client) SearchTracks(query string, onlyDownloadable bool) ([]Track, error) {
	URL, err := c.ServerEndpointURL("/v1/tracks/search")
	if err != nil {
		return nil, err
	}
	URL.AddQueryParameter("query", query)
	if onlyDownloadable {
		URL.AddQueryParameter("only_downloadable", "true")
	}

	return c.readTracks(URL)
}
